(function($) {
	$.fn.ChampGrid = function( options ) {
		var settings = $.extend({
			container  	: $(this),
			items   	: ".item",
			columns    	: 3,
			gutter		: "5px",
			callBack	: null
		}, options);
		var positions = {}; 
		var heights = 0;
		var item = $(settings.items);
		var column = settings.columns;
		var gutter = settings.gutter;
		var window_size = $(window).width();
		if(window_size > 568 && window_size <= 768){
			column = 2;				
		} else if(window_size < 568){
			column = 1;
		}
		getPostion = function (element){
			if (element.length) {	
			    positions.top = element.position().top;
			    positions.left = element.position().left;
			    positions.bottom = positions.top + element.outerHeight();
			    positions.right = positions.left + element.outerWidth();
			}
		}
		var containerWidth = settings.container.width();
		var singleColumnWidth = (containerWidth/column).toFixed(2);
		item.css('position','absolute');
		item.css('width',singleColumnWidth);
		var no_items = item.length;
		struct = function (cols){
			var col = cols;
			for(var i = 1;i<=no_items;i++){
				if(col > 1){
					if(i%col === 1){
						item.eq(i-1).attr('data-row', 'new');	
					} else {
						item.eq(i-1).removeAttr('data-row');	
					}
				} else if (col === 1){
					item.eq(i-1).attr('data-row', 'new');	
				} else {
					alert("Error..!");
				}	
			}
		}
		struct(column);
		item.each(function(i,el){
			var link = $(el);	
			getPostion($(el).prev());
			var attr = $(el).attr('data-row');
			if (typeof attr !== typeof undefined && attr !== false) {
				$(el).css('left','0');
				getPostion($(el).prevAll().eq((column-1)));
				$(el).css('top',positions.bottom);
				heights = heights + $(el).outerHeight();
			} else {
				$(el).css('left',positions.right);	
				if($(el).prevAll().eq((column)).length !== 0){
					getPostion($(el).prevAll().eq((column-1)));
					$(el).css('top',positions.bottom);		
				}
			}
			for(var j = 0;j<column;j++){
				item.eq(j).css('top','0');	
			}
		});
		$(settings.container).css('height',heights);
		item.css('border',gutter);
	}  
}(jQuery));
